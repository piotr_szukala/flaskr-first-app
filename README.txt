Przed uruchomieniem aplikacji trzeba utworzyć bazę dnych wykonując polecenie w IDLE:
>>> from flaskr import init_db
>>> init_db()

Aplikację uruchamiamy za pomocą pythona:
>>>python flaskr.py

Możemy podejrzeć aplikacje pod localhost:5000