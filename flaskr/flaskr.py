# all the imports
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash
# Potrzebne do utworzeneia funkcji, ktora inicjuje polaczenie z baza danych
from contextlib import closing

# configuration
DATABASE = '/tmp/flaskr.db'
# Uzywac trybu debug tylko przy budowanie aplikacji. Pozniej trzeba to koniecznie zmienic na Fasle
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'default'

# create our little application :)
'''Odwoluje sie do tego samego pliku szukajac zmeinnych UPPERCASE.
Jesli chcailbym przechowywac zmienne w innym pliku to musialbym uzyc opcji'''
#app.config.from_envvar('FLASKR_SETTINGS', silent=True)
app = Flask(__name__)
app.config.from_object(__name__)

# Zdefiniowane polaczenie z baza danych i wykonywanie sql
def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

# Funkcja inicjujaca polaczenie z baza danych i uruchamia skrypt sql
def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

# Dekorator jest wykonywany przy wywolaniu fukcji app. Metda before_request wywoluje te funkce zawsze przed zapytaniem sql
@app.before_request
def before_request():
    g.db = connect_db()

# Wykonywany zawsze po zapytaniu i bada czy jest exception . Jesli tak to omija zapytanie a jesli baza danych nie jest pusta to zayka ja.
@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

# Dodanie pierwszego widoku aplikacji, ktory pokazuje jako wszystkie wpisy z tabeli entries jako tuples. Uruchamia show_entries.html jako szablon
@app.route('/')
def show_entries():
    cur = g.db.execute('select title, text from entries order by id desc')
    entries = [dict(title=row[0], text=row[1]) for row in cur.fetchall()]
    return render_template('show_entries.html', entries=entries)

# Widok pozwalajacy zalogowanym uzytkownikom dodawac wpisy do tabeli entries
@app.route('/add', methods=['POST'])
def add_entry():
    if not session.get('logged_in'):
        abort(401)
    g.db.execute('insert into entries (title, text) values (?, ?)',
                 [request.form['title'], request.form['text']])
    g.db.commit()
    flash('New entry was successfully posted')
    return redirect(url_for('show_entries'))

# Pozwala zalogowac sie do bazy danych na podstawie zdefioniowanych zmiennych na poczatku pliku. trik polega na tym ze klucz logged_in zmiania sie na True
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)

# Funkcja wylogowania usuwa klucz logged_in i przyjmuje wartosc False.
@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))


# Fragment odpowiedzialny za uruchomienie serwera stanalone
if __name__ == '__main__':
    # Trzeba podmienic te wartosci w run() jesli chcemy zeby strona byla dostepna z zewnetrznej sieci
    # app.run(host='0.0.0.0')
    app.run()

    #Step 4
