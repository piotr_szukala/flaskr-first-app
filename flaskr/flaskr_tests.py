# Plik ustawiajacy testy aplikacju
import os
import flaskr
import unittest
import tempfile





class FlaskrTestCase(unittest.TestCase):

# Wlacza klienta testow (ten plik uruchamia sie osobno pythonem) Tworzy kopie bazy danych o losowej nazwie- mkstemp(). Dodawana jest flaga TESTING, ktora wylacza wyswietlanie errorow co powoduej ze lepiej widac co sie dzieje
    def setUp(self):
        self.db_fd, flaskr.app.config['DATABASE'] = tempfile.mkstemp()
        flaskr.app.config['TESTING'] = True
        self.app = flaskr.app.test_client()
        flaskr.init_db()


# Zamyka kopie bazy danych i usuwa ja z dysku
    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(flaskr.app.config['DATABASE'])

# Umozliwia wykonywanie testow tylko zalogowanym uzytkownikom. To umozliwia testowanie systemu logowania. Poniewaz login i logout to przekierowania to dodaje sie follow_redirects
    def login(self, username, password):
        return self.app.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

# Pierwszy test, ktory wysyla zapytanie do lokalizacji / do aplikacji i odbiera obiekt response_class jako odpowiedz. Mozna to odebrac metoda data.Test sprawdza, czy znajduje sie str 'No entries here so far'. Funkcje, ktore zaczynaja sie jako test sa interpretowane jako testy
    def test_empty_db(self):
        rv = self.app.get('/')
        assert 'No entries here so far' in rv.data

# Nowy test sprwdzajacy system logowania
    def test_login_logout(self):
        rv = self.login('admin', 'default')
        assert 'You were logged in' in rv.data
        rv = self.logout()
        assert 'You were logged out' in rv.data
        rv = self.login('adminx', 'default')
        assert 'Invalid username' in rv.data
        rv = self.login('admin', 'defaultx')
        assert 'Invalid password' in rv.data


    # Test wprowadzajacy rekordy do bazy danych
    def test_messages(self):
        self.login('admin', 'default')
        rv = self.app.post('/add', data=dict(
            title='<Hello>',
            text='<strong>HTML</strong> allowed here'
        ), follow_redirects=True)
        assert 'No entries here so far' not in rv.data
        assert '&lt;Hello&gt;' in rv.data
        assert '<strong>HTML</strong> allowed here' in rv.data


if __name__ == '__main__':
    unittest.main()
